﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variables_1
{
	class Program
	{
		static void Main(string[] args)
		{
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Here goes all we need for now - the program will execute code inside these brackets in the Main method//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////

			//The standard output to the console.
			Console.WriteLine("Hello World!");

			//Declaration and value assignment of an integer variable.
			int integerVariable = 5;

			//Declarations and value assignments of a floating variables.
			//Notice the necessary 'f' to convert the value to float type.
			double floatingVariable = 3.14;
			float anotherFloatingVariable = 14.5f;

			//Declaration and value assignement of a character variable.
			char characterVariable = 's';

			//Declaration and value assignement of a text variable.
			string textVariable = "I love variables like: ";

			//Declaration and value assignement of a boolean variable.
			//The only possible values are "true" or "false".
			bool booleanVariable = true;

			//Displaying variables using the standard output.
			//Notice we start the counting from 0.
			Console.WriteLine("{0} {1}, {2}, {3}, {4}, {5}",
							  textVariable, integerVariable, floatingVariable, anotherFloatingVariable, characterVariable, booleanVariable);

			//Disclaimer: C# allows us to expand command to many lines. The end of command is ';' not "Enter" key.

			Console.WriteLine("Please type something and hit enter to end the program:");

			//The standard input from user. Use it at the end so the program won't shut down immediately
			Console.ReadKey();
		}
	}
}
