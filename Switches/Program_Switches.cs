﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Switches
{
	class Program
	{
		static void Main(string[] args)
		{
			/////////////////////////////////////////////////////////////////////////////////////////////////
			//Switch construction is used to execute different parts of code, depending on a variable value//
			/////////////////////////////////////////////////////////////////////////////////////////////////

			bool booleanVariable = true;
			int integerVariable = 50;

			//The "break" keyword separates the cases.
			switch (booleanVariable)
			{
				case true:
					Console.WriteLine("The variable equals true!");
					break;
				case false:
					Console.WriteLine("The variable equals false!");
					break;
			}

			Console.WriteLine();

			//If we cannot write down all of the cases, we can use "default" keyword.
			//The code from default case is executed only if the variable value does not match other cases.
			switch (integerVariable)
			{
				case 3:
					Console.WriteLine("The code from 3 is being executed");
					break;
				case 2:
					Console.WriteLine("The code from 2 is being executed");
					break;
				default:
					Console.WriteLine("The integer variable value is neither 2 nor 3. Executing the default case");
					break;
			}

			Console.WriteLine();



			//////////////////////////////////////////////////////////////
			//Simple calculator using switch and operations on variables//
			//////////////////////////////////////////////////////////////

			bool exitProgram = false;
			int userChoice = 0;

			double numberOne = 0;
			double numberTwo = 0;
			double result = 0;

			while (!exitProgram)
			{
				Console.WriteLine("Welcome to the calculator! What do you want to do?");
				Console.WriteLine("1. Add two numbers,");
				Console.WriteLine("2. Substract two numbers,");
				Console.WriteLine("3. Multiply two numbers,");
				Console.WriteLine("4. Divide two numbers,");
				Console.WriteLine("5. Exit.");

				userChoice = int.Parse(Console.ReadLine());

				//Clear the console output.
				Console.Clear();

				switch (userChoice)
				{
					//For all the cases 1-4 get input from user.
					//With this construction one code is being applied to all of these cases.
					case 1:
					case 2:
					case 3:
					case 4:
						Console.WriteLine("Please input the number #1:");
						numberOne = double.Parse(Console.ReadLine());
						Console.WriteLine("Please input the number #2:");
						numberTwo = double.Parse(Console.ReadLine());
						break;
					//End the loop iteration and end the program.
					case 5:
						exitProgram = true;
						continue;
					//Display feedback and go to the next iteration.
					default:
						Console.WriteLine("Something went wrong! Try another number.");
						continue;
				}

				//Action depending on a choice.
				switch (userChoice)
				{
					case 1:
						result = numberOne + numberTwo;
						break;
					case 2:
						result = numberOne - numberTwo;
						break;
					case 3:
						result = numberOne * numberTwo;
						break;
					case 4:
						result = numberOne / numberTwo;
						break;
				}

				Console.WriteLine("The result equals {0}. Hit enter to go back to main menu.", result);
				Console.ReadLine();
				Console.Clear();
			}

			///////////
			//The end//
			///////////

			//No ReadLine this time :P
		}
	}
}
