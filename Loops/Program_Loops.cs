﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
	class Program
	{
		static void Main(string[] args)
		{
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			//Loops let you iterate through the same code many times. The syntax is very simillar to IF condition//
			///////////////////////////////////////////////////////////////////////////////////////////////////////



			/////////////////////////////////////////////////////////////////////////////
			//FOR loop iterates by some step and till condition defined in the brackets//
			/////////////////////////////////////////////////////////////////////////////

			//This loop is counting from 1 to 10 inclusively.
			//The "i++" is equal to "i += 1" or "i = i+1".
			for (int i = 1; i <= 10; i++)
			{
				Console.Write("{0} ", i);
			}
			Console.WriteLine();

			//This loop is counting from 10 to 0 excluded, but with the step equal 2.
			for (int i = 10; i > 0; i -= 2)
			{
				Console.Write("{0} ", i);
			}
			Console.WriteLine();

			//You can also use the "continue" keyword if you want to skip an iteration.
			//This loop is counting from 10 to 0 excluded, with step equal 2, but done by using the "continue" keyword.
			//The "i--" is equal to "i -= 1" or "i = i-1".
			for (int i = 10; i > 0; i--)
			{
				//The "%" operation returns a rest from division. In this case we check, if the value of i is odd.
				if (i % 2 == 1)
				{
					continue;
				}

				Console.Write("{0} ", i);
			}
			Console.WriteLine();

			Console.WriteLine();



			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//The WHILE loop is just like IF condition, but the code is executed as long, as the condition is fulfilled//
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////

			int numberToGuess = 31;
			int userGuess = 0;

			//The "!=" comparison means "is different from". In this case, it is not true only if the user guess the number.
			while (userGuess != numberToGuess)
			{
				Console.WriteLine("Please guess an integer:");
				userGuess = int.Parse(Console.ReadLine());
			}

			Console.WriteLine("Contratulation! You guessed the number: {0}", numberToGuess);
			Console.WriteLine();

			//You can also use the "break" keyword to immediately exit the loop.
			//The upcoming example works same as the one up, but allows us to give more polish to the user experience.
			numberToGuess = 311;
			userGuess = 0;

			//Give user the task only once.
			Console.WriteLine("Please guess an integer:");

			//The loop lasts forever! No way to exit from it, except for "break" keyword;
			while (true)
			{
				userGuess = int.Parse(Console.ReadLine());

				//The "==" comparison means "is equal to".
				if (numberToGuess == userGuess)
				{
					break;
				}

				Console.WriteLine("Wrong guess! Try one more time:");
			}

			Console.WriteLine("Contratulation! You guessed the number: {0}", numberToGuess);
			Console.WriteLine();



			///////////////////////////////////////////////////////////////////////////////////////////////
			//The DO WHILE loop is same as WHILE, but the condition is checked after the code in brackets//
			///////////////////////////////////////////////////////////////////////////////////////////////

			//These are the same, but the loop will run at least once.
			numberToGuess = 13;
			userGuess = 13;

			do
			{
				Console.WriteLine("Please guess an integer:");
				userGuess = int.Parse(Console.ReadLine());
				//This one's tricky. You need to put a semicolon after the condition brackets.
			} while (userGuess != numberToGuess);

			Console.WriteLine("Contratulation! You guessed the number: {0}", numberToGuess);
			Console.WriteLine();



			///////////
			//The end//
			///////////

			Console.ReadLine();
		}
	}
}
