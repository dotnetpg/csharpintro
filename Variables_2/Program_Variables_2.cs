﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variables_2
{
	class Program
	{
		static void Main(string[] args)
		{
			////////////////////////////////////////////////////////////////////
			//Declaration of some variables and displaying them in the console//
			////////////////////////////////////////////////////////////////////

			int integerVariable = 5;
			double floatingVariable = 3.141592;
			float anotherFloatingVariable = 21.37f;
			double result = 0;

			Console.WriteLine("Int: {0}, double: {1}, float: {2}", integerVariable, floatingVariable, anotherFloatingVariable);

			//Skip one line:
			Console.WriteLine();



			///////////////////////////////////////////
			//Standard operations on number variables//
			///////////////////////////////////////////

			//Add the variables.
			Console.WriteLine("Result: {0}, add {1}", result, integerVariable);
			result += integerVariable;
			Console.WriteLine("Result: {0}, add {1}", result, integerVariable);
			//The other way to do it (goes the same for difference, multiply and divide).
			result = result + integerVariable;

			//Substract the variables.
			Console.WriteLine("Result: {0}, minus {1}", result, floatingVariable);
			result -= floatingVariable;

			//Multiply the variables.
			Console.WriteLine("Result: {0}, multiply by {1}", result, anotherFloatingVariable);
			result *= anotherFloatingVariable;

			//Divide the variables.
			Console.WriteLine("Result: {0}, divide by {1}", result, 25);
			result /= 25;
			Console.WriteLine("Result: {0}", result);
			Console.WriteLine();



			//////////////////////////////////////
			//The negation of a boolean variable//
			//////////////////////////////////////

			bool booleanVariable = true;
			Console.WriteLine("Boolean: {0}", booleanVariable);
			booleanVariable = !booleanVariable;
			Console.WriteLine("Negated boolean: {0}", booleanVariable);
			Console.WriteLine();



			/////////////////////////////////////////////////////
			//Adding text and characters to the string variable//
			/////////////////////////////////////////////////////

			string textVariable = "Hello world!";
			string anotherTextVariable = "ABCD-";
			char characterVariable = 'E';
			Console.WriteLine("String: {0}", textVariable);
			textVariable += ' ' + anotherTextVariable + characterVariable + "-F. That's all!";
			Console.WriteLine("String: {0}", textVariable);



			///////////
			//The end//
			///////////

			Console.ReadLine();
		}
	}
}
