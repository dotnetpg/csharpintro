﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
	class Program
	{
		static void Main(string[] args)
		{
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Arrays are series of variables of some type. You don't have to declare all the variables you need, just put a value in an array//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//Declare new integer array with 10 elements.
			int[] integerArray = new int[10];

			//Assign some array elements between inclusive 0 and 9, as we have 10 elements.
			Console.Write("Please enter value #0: ");
			integerArray[0] = int.Parse(Console.ReadLine());

			Console.Write("Please enter value #3: ");
			integerArray[3] = int.Parse(Console.ReadLine());

			Console.Write("Please enter value #9: ");
			integerArray[9] = int.Parse(Console.ReadLine());

			Console.WriteLine();

			//Display the array using FOR loop.
			for (int i = 0; i < 10; i++)
			{
				//Get access to array element at index "i" (minimally 0 and maximally 9 - we begin counting from 0).
				Console.WriteLine("Integer Array element #{0}: {1},", i, integerArray[i]);
			}
			Console.WriteLine();



			//////////////////////////////////////////////////////////////////////////////////////
			//To assign some values at declaration use the { } brackets after declaring an array//
			//////////////////////////////////////////////////////////////////////////////////////

			double[] floatingArray = new double[3] { 3.14, 21.37, 9.11 };

			//As you can see, if you add values at declaration, you don't have to assign a size of the array.
			//The following example will have 5 elements.
			char[] characterArray = new char[] { 'a', 'b', 'c', 'd', 'e' };

			//To iterate through all the elements of an array, no matter the size, use "array.Length" property.
			//This loop will have 3 iterations.
			for (int i = 0; i < floatingArray.Length; i++)
			{
				Console.WriteLine("Floating Array element #{0}: {1},", i, floatingArray[i]);
			}
			Console.WriteLine();

			//This loop will have 5 iterations.
			for (int i = 0; i < characterArray.Length; i++)
			{
				Console.WriteLine("Character Array element #{0}: {1},", i, characterArray[i]);
			}
			Console.WriteLine();



			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//As you may guessed already, string variable is an array of characters and you can use it like an array//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////

			//"Hello World!" string as an array with 12 elements.
			string stringVariable = "Hello World!";

			Console.WriteLine("{0} as an array:", stringVariable);

			Console.WriteLine("1st letter: {0}", stringVariable[0]);
			Console.WriteLine("3rd letter: {0}", stringVariable[2]);
			Console.WriteLine("11th letter: {0}", stringVariable[10]);

			Console.WriteLine("Array length: {0}", stringVariable.Length);

			Console.WriteLine();

			///////////
			//The end//
			///////////

			Console.ReadLine();
		}
	}
}
