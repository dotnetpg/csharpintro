﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
	class Program
	{
		static void Main(string[] args)
		{
			////////////////////////////////////////////////
			//IF Condition Syntax: if ( booleanValue ) { }//
			////////////////////////////////////////////////

			if (true)
			{
				Console.WriteLine("This code is being executed..");
			}

			if (false)
			{
				Console.WriteLine("..and this code ain't");
			}



			//////////////////////////////////////////////////////////////
			//The boolean value inside the condition can be a comparison//
			//////////////////////////////////////////////////////////////

			Console.WriteLine("Please enter your height in centimeters: ");

			//Parsing the input from the user to integer value.
			int height = int.Parse(Console.ReadLine());

			//If height is more or equal to 170.
			if (height >= 170)
			{
				Console.WriteLine("You're tall!");
			}

			//If height is less than 170.
			if (height < 170)
			{
				Console.WriteLine("You're short!");
			}

			Console.WriteLine();


			///////////////////////////////////////////////////////////////////////////////////////////
			//If the conditions are excluding each other, we can use the else or else if construction//
			///////////////////////////////////////////////////////////////////////////////////////////

			Console.WriteLine("Please enter your height in centimeters: ");
			height = int.Parse(Console.ReadLine());

			//If height is more or equal to 180.
			if (height >= 180)
			{
				Console.WriteLine("You're tall!");
			}
			//If height is less than 180, but more than 160.
			else if (height > 160)
			{
				Console.WriteLine("You're mid!");
			}
			//If height is less or equal to 160.
			else
			{
				Console.WriteLine("You're short!");
			}

			Console.WriteLine();


			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			//The conjuction and alternative can be used in conditions, and are represented respectively as && and ||//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////

			Console.WriteLine("Please enter your height in centimeters: ");
			height = int.Parse(Console.ReadLine());

			//If height is more or equal to 180 OR is less or equal to 160.
			if ((height >= 180) || (height <= 160))
			{
				Console.WriteLine("You're either very tall, or very short!");
			}

			//If height is less or equal to 180 AND is more or equal to 160.
			if ((height < 180) && (height > 160))
			{
				Console.WriteLine("You're not too high and not too short!");
			}

			Console.WriteLine();



			///////////
			//The end//
			///////////

			Console.ReadLine();
		}
	}
}
